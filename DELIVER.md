#### Explicação sobre as decisões de arquitetura;
*A facilidade de entendimento e facilidade de manutenção foi o principal motivo da utilização dessa arquitetura.*

#### O que você faria diferente se tivesse mais tempo;
*Considerando um projeto real, a necessidade de um webserver para tratar todas as requisições direcionadas a API do google seria de bom proveito ou ao menos para a economia de requisições para as imagens, pois o limite de mil requisições diarias seriam atingidas brevemente na forma atual.
Outro detalhe que seria bem vindo no futuro seria uma continuidade para proximas paginas de listagem e por fim a implementação de rotas dentro do aplicativo se o esforço realmente fosse necessario ignorar as preferencias do usuario em utilizar o aplicativo de mapas preferenciais.*

#### Caso não tenha entregado tudo o que foi solicitado, explicar o motivo.
...