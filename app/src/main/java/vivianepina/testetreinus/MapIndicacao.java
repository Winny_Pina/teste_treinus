package vivianepina.testetreinus;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapIndicacao extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LatLng posicaoUsuario;
    private LatLng posicaoLugar;
    private String nome;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_indicacao_activity);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        posicaoUsuario = new LatLng(getIntent().getDoubleExtra("SADDR_LAT",0), getIntent().getDoubleExtra("SADDR_LNG",0));
        posicaoLugar = new LatLng(getIntent().getDoubleExtra("DADDR_LAT",0), getIntent().getDoubleExtra("DADDR_LNG",0));
        nome = getIntent().getStringExtra("NOME");

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(6.0f);
        mMap.setMaxZoomPreference(20.0f);
        mMap.addMarker(new MarkerOptions().position(posicaoUsuario).title("Você")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        mMap.addMarker(new MarkerOptions().position(posicaoLugar).title(nome));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(posicaoLugar));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(13), 2000, null);
    }
}
