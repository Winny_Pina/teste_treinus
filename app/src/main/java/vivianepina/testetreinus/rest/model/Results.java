package vivianepina.testetreinus.rest.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;
import java.util.List;

@JsonObject
public class Results {
    @JsonField(name = "results")
    private List<Indicado> indicados;

    public List<Indicado> getIndicados() {
        return indicados;
    }

    public void setIndicados(List<Indicado> indicados) {
        this.indicados = indicados;
    }
}
