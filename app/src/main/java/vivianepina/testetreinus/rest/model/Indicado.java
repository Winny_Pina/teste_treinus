package vivianepina.testetreinus.rest.model;

import android.support.annotation.NonNull;
import android.util.Log;
import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;

@JsonObject
public class Indicado implements Comparable{


    public Indicado(String name, String icon) {
        this.name = name;
        this.icon = icon;
    }

    public Indicado() {}

    @JsonField
    String name;
    @JsonField
    String icon;
    @JsonField(name = "place_id")
    String placeID;
    @JsonField
    String vicinity;
    @JsonField
    Geometry geometry;
    @JsonField
    List<Photos> photos;

    Double latOrigem;
    Double lngOrigem;


    double distancia = 0;

    public String getPlaceID() {
        return placeID;
    }

    public void setPlaceID(String placeID) {
        this.placeID = placeID;
    }

    public double getDistancia() {
        return distancia;
    }

    private void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public List<Photos> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photos> photos) {
        this.photos = photos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public double calculateDistance(double lat_a, double lng_a){

        latOrigem = lat_a;
        lngOrigem = lng_a;

        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(getGeometry().getLocation().getLat()-lat_a);
        double lngDiff = Math.toRadians(getGeometry().getLocation().getLng()-lng_a);
        double a = Math.sin(latDiff /2) * Math.sin(latDiff /2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(getGeometry().getLocation().getLat())) *
                        Math.sin(lngDiff /2) * Math.sin(lngDiff /2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = earthRadius * c;

        int meterConversion = 1609;
        setDistancia(distance * meterConversion);
        return getDistancia();
    }

    public LatLng getOrigem(){
        return new LatLng(latOrigem, lngOrigem);
    }

    public LatLng getPosition(){
        return new LatLng(getGeometry().getLocation().getLat(), getGeometry().getLocation().getLng());
    }

    @Override
    public int compareTo(@NonNull Object indicado) {
        Log.e("t", "teste");
        return (int)(this.getDistancia() - ((Indicado)indicado).getDistancia());
    }
}
