package vivianepina.testetreinus.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import vivianepina.testetreinus.rest.model.Results;

public interface GooglePlaceAPI {


    @GET("/maps/api/place/nearbysearch/json?type=gym&radius=50000")
    Call<Results> getPlaces(@Query("location") String location, @Query("key") String key);
}
