package vivianepina.testetreinus;

import android.Manifest;
import android.Manifest.permission;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vivianepina.testetreinus.adapter.IndicadosAdapter;
import vivianepina.testetreinus.rest.GooglePlaceAPI;
import vivianepina.testetreinus.rest.model.Indicado;
import vivianepina.testetreinus.rest.model.Results;

@EActivity(R.layout.lista_indicacao_activity)
public class IndicationActivity  extends AppCompatActivity {

    static final int PERMISSAO_PARA_USAR_GPS = 5165 ;
    String TAG = this.getClass().getSimpleName();
    @ViewById
    RecyclerView list;
    @ViewById
    TextView empty_view;
    UserPositionService mUserPosition;

    @Bean
    IndicadosAdapter adapter;

    @AfterViews
    void bindAdapter() {

        List<Indicado> lista = new ArrayList<>();

        adapter.setList(lista);

        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
        alphaAdapter.setInterpolator(new OvershootInterpolator());

        list.setAdapter(alphaAdapter);

        list.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        adapter.notifyDataSetChanged();

        Log.d(TAG, "Tamanho Da lista " + adapter.getItemCount());

        if (ContextCompat.checkSelfPermission(this,
                permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                Toast.makeText(this, "É necessario acesso ao GPS para que tudo funcione. Por favor reconsidere a permissão de acesso", Toast.LENGTH_LONG).show();

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSAO_PARA_USAR_GPS);
            }
        } else {
            Intent intent = new Intent(getApplicationContext(), UserPositionService.class);
            startService(intent);
        }


    }


    private void loadData(final double lat, final double lng)
    {
        GooglePlaceAPI api = ((App) getApplication()).googlePlaceAPI();
        Call<Results> call = api.getPlaces((lat+","+lng), getString(R.string.google_api_key));

        call.enqueue(new Callback<Results>() {
            @Override
            public void onResponse(Call<Results> call, Response<Results> response) {
                for (Indicado indicado: response.body().getIndicados()) {
                    indicado.calculateDistance(lat,lng);
                }
                Collections.sort(response.body().getIndicados());
                adapter.setList(response.body().getIndicados());
                if(adapter.getItemCount() > 0){
                    list.setVisibility(View.VISIBLE);
                    empty_view.setVisibility(View.GONE);
                } else {
                    list.setVisibility(View.GONE);
                    empty_view.setVisibility(View.VISIBLE);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Results> call, Throwable t) {

            }
        });

    }

    private Double latitude;
    private Double longitude;

    @Override
    public void onRequestPermissionsResult(int requestCode,
            String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSAO_PARA_USAR_GPS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(getApplicationContext(), UserPositionService.class);
                    startService(intent);

                } else {

                    Toast.makeText(this, "É necessario acesso ao GPS para que tudo funcione. Por favor reconsidere a permissão de acesso", Toast.LENGTH_LONG).show();
                }
            }

        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            latitude = intent.getDoubleExtra("latitude", 0);
            longitude =intent.getDoubleExtra("longitude", 0);

            loadData(latitude, longitude);

            Log.d(TAG, latitude +"," + longitude);
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter("teste12312312313"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }




}
