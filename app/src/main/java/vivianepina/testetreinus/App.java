package vivianepina.testetreinus;

import android.app.Application;
import com.github.aurae.retrofit2.LoganSquareConverterFactory;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import vivianepina.testetreinus.rest.GooglePlaceAPI;

public class App extends Application {

    private Retrofit getRetrofit(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(50, TimeUnit.SECONDS);
        httpClient.addInterceptor(interceptor);

        httpClient.addNetworkInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                final Request request = chain.request().newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept", "application/json")
                        .addHeader("OS", "Android")
                        .addHeader("LOCALE", "pt-BR")

                        .build();
                return chain.proceed(request);
            }
        });
        HttpLoggingInterceptor interceptor2 = new HttpLoggingInterceptor();
        interceptor2.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addNetworkInterceptor(interceptor2);

        return new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com")
                .addConverterFactory(LoganSquareConverterFactory.create())
                .client(httpClient.build())
                .build();
    }

    public GooglePlaceAPI googlePlaceAPI(){
        return getRetrofit().create(GooglePlaceAPI.class);
    }

}
