package vivianepina.testetreinus.adapter.holder;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import vivianepina.testetreinus.R;
import vivianepina.testetreinus.rest.model.Indicado;

@EViewGroup(R.layout.indicado_row)
public class IndicadoViewHolder  extends LinearLayout {

    @ViewById
    TextView  nome;
    @ViewById
    TextView  endereco;
    @ViewById
    TextView  distancia;
    @ViewById
    ImageView icon;
    Context context;
    public IndicadoViewHolder(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(Indicado indicado){
        nome.setText(indicado.getName());
        endereco.setText(indicado.getVicinity());
        int dist = (int)indicado.getDistancia();
        if(dist > 2000){
            dist = dist / 1000;
            distancia.setText("a "+ dist +"km de distância");
        } else {
            distancia.setText("a "+ dist +"m de distância");
        }
        if(indicado.getPhotos() != null && indicado.getPhotos().size() > 0){
            Log.e("A", indicado.getPhotos().get(0).getPhoto_reference());

            Picasso.with(context).load("https://maps.googleapis.com/maps/api/place/photo?maxwidth="+
                    indicado.getPhotos().get(0).getWidth()+"&photoreference="+
                    indicado.getPhotos().get(0).getPhoto_reference()+"&key=AIzaSyCyJdC7KzxTd11AwM2VfZTUbkApO7vJSN4")
                    .error(R.mipmap.ic_launcher_round)
                    .placeholder(R.drawable.circular_progress_bar)
                    .into(icon);


        }
    }





}
