package vivianepina.testetreinus.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import vivianepina.testetreinus.MapIndicacao;
import vivianepina.testetreinus.adapter.holder.IndicadoViewHolder;
import vivianepina.testetreinus.adapter.holder.IndicadoViewHolder_;
import vivianepina.testetreinus.adapter.wrapper.RecyclerViewAdapterBase;
import vivianepina.testetreinus.adapter.wrapper.ViewWrapper;
import vivianepina.testetreinus.rest.model.Indicado;

@EBean
public class IndicadosAdapter extends RecyclerViewAdapterBase<Indicado, IndicadoViewHolder> {

    private String TAG = this.getClass().getSimpleName();
    @RootContext
    Context context;

    @Override
    protected IndicadoViewHolder onCreateItemView(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateItemView ");
        return IndicadoViewHolder_.build(context);
    }
    // Chave Google maps
    // AIzaSyCyJdC7KzxTd11AwM2VfZTUbkApO7vJSN4
    // Destino
    // https://www.google.com/maps?daddr=-20.0777204,-49.91672990000001&saddr=-20.0801417,-49.91194220000001
    //
    // Busca
    // https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-20.093337,-49.924622&type=gym&radius=10000&key=AIzaSyCyJdC7KzxTd11AwM2VfZTUbkApO7vJSN4

    @Override
    public void onBindViewHolder(ViewWrapper<IndicadoViewHolder> holder, final int position) {
        Log.d(TAG, "Requisitado " + position);
        IndicadoViewHolder view = holder.getView();
        final Indicado indicado = items.get(position);
        view.bind(indicado);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MapIndicacao.class);
                intent.putExtra("SADDR_LAT", indicado.getOrigem().latitude);
                intent.putExtra("SADDR_LNG", indicado.getOrigem().longitude);
                intent.putExtra("DADDR_LAT", indicado.getPosition().latitude);
                intent.putExtra("DADDR_LNG", indicado.getPosition().longitude);
                intent.putExtra("NOME", indicado.getName());
                context.startActivity(intent);
            }
        });
    }
}
